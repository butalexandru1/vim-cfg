syntax on

set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

Plugin 'rafi/awesome-vim-colorschemes'

Plugin 'ericbn/vim-relativize'
Plugin 'Valloric/YouCompleteMe'
Plugin 'scrooloose/nerdtree'
Plugin 'ap/vim-buftabline'

set number relativenumber

call vundle#end()            " required
filetype plugin indent on    " required

colorscheme deus

" Set nerdtree open by defautl and toggle by F3
" autocmd vimenter * NERDTree
nmap <F3> :NERDTreeToggle<CR>

" Buffer navigation with vim-buftabline
set hidden
nnoremap <C-N> :bnext<CR>
nnoremap <C-P> :bprev<CR>
nnoremap <C-D> :bdelete<CR>

" Switch between opened windows using C-J C-K
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

set hlsearch

set tabstop=4
set shiftwidth=4
set expandtab

set spell
augroup twig_ft
  au!
  autocmd BufNewFile,BufRead *.pddl   set syntax=lisp
augroup END
