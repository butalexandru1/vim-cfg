#/bin/bash
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
cp ./.vimrc ~/.vimrc
mkdir -p ~/.vim/colors
wget https://raw.githubusercontent.com/ajmwagar/vim-deus/master/colors/deus.vim -O ~/.vim/colors/deus.vim
